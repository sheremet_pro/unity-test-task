# Repository info

Unity test task.
Made with Unity 2019.3.0f6.

# Choice of general architecture approach

> The end result for the test is to have a system for MonoBehaviours to request their distances and occlusion status relative to a reference point (setted in run-time, i.e. a player).

The only thing we need from reference point is its position, therefore it would be the best to simply make it Vector3.

> MonoBehaviours should then be able to act accordingly to this information.
> As an example, an AI agent would deploy different behaviour(s) when
> - distant from its target, and/or
> - looking away from the reference point, and/or
> - occluded (not visible on screen)

A system with subscription on some events (`onDistanceChange`, `onIsOccludedChange`, etc) via wrapping methods (e.g. `tracker.SubscribeOnDistanceChange(Transform referencePoint, Action onChange)`) might work but:
1. Reference point can't be Vector3 anymore, it should be at least Transform so Tracker can store it and check for its position updates itself. What if we want to check against a point in space where there is no Transform?
2. Conditions can depend on each other. We may want to check e.g. for `distance >= 10 AND occlusion == true`. This approach doesn't give us proper flexibility to do that.
3. We may want checks to happen non-regularly. For performance some of uses could be pretty rare (e.g. once per second instead of once per frame, or at the start and at the quest finish - twice per 10 minutes). This approach is not flexible enough for that. Only user of this system knows how often he wants to check conditions.

The solution for the described problems is to have a couple of condition check methods that caller will be able to use when he wants and to interpret result of calls how he wants even for complex conditions.

## Where to place condition check methods

We may create a MonoBehaviour component, place condition check methods there and ask callers to somehow get a reference to the component before use.
This will work but since we can simply pass all the required data in condition check methods, we'll not create a special component but use a static class with static methods instead. For convenience, let's make them to be extensions for MonoBehaviour.

# Implementation

From the task text, we should have 3 main condition check methods:
1. Distance check
2. "Looking away" check
3. Occlusion check

## Distance

Distance between caller and reference point.
Simple Vector3.Distance should work here just fine.

## "Looking away"

It's required to find an angle between caller's look direction and reference point position.
Once we know the angle and caller's "cone of sight" angles, we compare them and answer if they intersect.

## Occlusion check

As I understand, it's required to check whether caller's collider is visible from a given reference point or not.
Without knowing any special criterias (e.g. caller's collider is much bigger than obstacles or high accuracy is required) the best way is to send some rays from the reference point and see if any of them reach the caller's collider.

Accuracy is a key here: raycast is not cheap in terms of performance. In different situations we may want different amount of accuracy, therefore there should be a parameter for number of rays.
