﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Rotator : MonoBehaviour
{
    [SerializeField] private float rotationSpeed = 60;

    private Rigidbody rb;

    private void FixedUpdate()
    {
        var deltaAngleY = rb.rotation.eulerAngles.y + rotationSpeed * Time.fixedDeltaTime;
        var rotation = Quaternion.Euler(0, deltaAngleY, 0);
        rb.MoveRotation(rotation);
    }

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }
}
