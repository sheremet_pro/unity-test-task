﻿using UnityEngine;
using UnityEngine.UI;
using VisibilityTrackLib;

public class UsageExampleScript : MonoBehaviour
{
    [Header("General")]
    [SerializeField] private new Collider collider;
    [SerializeField] private Transform referenceTransform;

    [Header("Distance")]
    [SerializeField] private Text distanceText;

    [Header("'Looking at'")]
    [SerializeField] private float coneOfSightAngle = 60;
    [SerializeField] private Text lookingAtPointText;

    [Header("Occlusion")]
    [SerializeField] private LayerMask layerMask;
    [SerializeField] private Text visibleFromPointText;
    [SerializeField] private OcclusionCheckAccuracy accuracy;

    private void Update()
    {
        var referencePoint = referenceTransform.position;

        var distance = transform.Distance(referencePoint);
        distanceText.text = $"Distance: {distance}";

        var isLookingAtPoint = transform.IsLookingAt(referencePoint, coneOfSightAngle);
        lookingAtPointText.text = $"Looking at point: {isLookingAtPoint}";
        
        var isVisibleFromPoint = !collider.IsOccluded(
            referenceTransform.position, layerMask, accuracy);
        visibleFromPointText.text = $"Visible from point: {isVisibleFromPoint}";
    }
}
