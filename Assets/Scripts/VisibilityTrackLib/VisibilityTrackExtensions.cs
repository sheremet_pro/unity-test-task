﻿using System;
using UnityEngine;

namespace VisibilityTrackLib
{
    public static class VisibilityTrackExtensions
    {
        public static float Distance(this Transform transform,
            Vector3 referencePoint)
        {
            return Vector3.Distance(transform.position, referencePoint);
        }

        public static float Angle(this Transform transform,
            Vector3 referencePoint)
        {
            var lookDir = transform.forward;
            var pointDir = referencePoint.normalized;
            return Vector3.Angle(lookDir, pointDir);
        }

        public static bool IsLookingAt(this Transform transform,
            Vector3 referencePoint,
            float coneOfSightAngle)
        {
            if (coneOfSightAngle <= 0)
            {
                throw new ArgumentOutOfRangeException("coneOfSightAngle");
            }

            var maxDeltaAngle = coneOfSightAngle / 2;
            return transform.Angle(referencePoint) <= maxDeltaAngle;
        }

        public static bool IsOccluded(this Collider collider,
            Vector3 referencePoint,
            LayerMask layerMask,
            OcclusionCheckAccuracy accuracy = OcclusionCheckAccuracy.Default)
        {
            switch (accuracy)
            {
                case OcclusionCheckAccuracy.Default:
                    return !collider.IsHit(referencePoint, collider.bounds.center, layerMask)
                        && !collider.IsHit(referencePoint, collider.bounds.min, layerMask)
                        && !collider.IsHit(referencePoint, collider.bounds.max, layerMask);
                case OcclusionCheckAccuracy.Low:
                    return !collider.IsHit(referencePoint, collider.bounds.center, layerMask);
                default:
                    throw new ArgumentException("accuracy");
            }
        }

        private static bool IsHit(this Collider collider,
            Vector3 startPoint,
            Vector3 colliderPoint,
            LayerMask layerMask)
        {
            var direction = colliderPoint - startPoint;
            var isHit = Physics.Raycast(
                startPoint, direction, out RaycastHit hit, direction.magnitude, layerMask)
                && hit.collider == collider;

            var debugRayColor = isHit
                ? Color.green
                : Color.red;
            Debug.DrawRay(startPoint, direction, debugRayColor);

            return isHit;
        }
    }

    public enum OcclusionCheckAccuracy
    {
        /// <summary>
        /// Maximum three rays will be used
        /// </summary>
        Default,
        /// <summary>
        /// Maximum one ray will be used
        /// </summary>
        Low,
    }
}


