﻿using System.Collections;
using UnityEngine;

public class PointToPointMovement : MonoBehaviour
{
    private const float OFFSET = 0.01f;

    [SerializeField] private Vector3[] points;
    [SerializeField] private float speed;

    private void Start()
    {
        if (points.Length > 0)
        {
            StartCoroutine(MoveRoutine(0));
        }
    }

    private IEnumerator MoveRoutine(int pointIndex)
    {
        if (pointIndex == points.Length)
        {
            pointIndex = 0;
        }

        var point = points[pointIndex];

        while (Vector3.Distance(transform.position, point) > OFFSET)
        {
            yield return new WaitForFixedUpdate();
            var maxDistanceDelta = speed * Time.fixedDeltaTime;
            transform.position = Vector3.MoveTowards(transform.position, point, maxDistanceDelta);
        }

        StartCoroutine(MoveRoutine(pointIndex + 1));
    }
}
